import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import Todoist from './services/todoist.js';
import EventBus from './services/eventbus';
import Options from './services/options';

const app = createApp(App)
    .provide('todoist', new Todoist)
    .provide('eventBus', new EventBus)
    .provide('options', new Options)
    .mount('#app');