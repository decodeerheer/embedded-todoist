export default class Options
{
    options;

    constructor()
    {
        this.options = {};
    }

    set(key, value)
    {
        return this.options[key] = value;
    }

    get(key, fallback)
    {
        return this.options[key] || fallback;
    }
}