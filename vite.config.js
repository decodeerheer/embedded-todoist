import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { viteSingleFile } from 'vite-plugin-singlefile';

// https://vitejs.dev/config/
export default defineConfig(({ command, mode }) => ({
  plugins: [vue(), viteSingleFile()],
  build: {
    emptyOutDir: true,
    minify: mode === 'development' ? false : true,
  }
}));
