import { TodoistApi } from "@doist/todoist-api-typescript";

export default class Todoist
{
    api;
    project;

    completeTimeout;
    syncInterval;

    setup(accessKey) {
        this.api = new TodoistApi(accessKey);

        return this;
    }

    getProject(projectId, options) {
        return new Promise(async (resolve, reject) => {
            try {
                if (window.localStorage.getItem(this.#localStorageName(projectId))) {
                    this.project = JSON.parse(
                        window.localStorage.getItem(this.#localStorageName(projectId))
                    );

                    resolve(this.project);
                }

                this.project = await this.api.getProject(projectId);
                let sections = await this.api.getSections(projectId);
                let tasks = await this.api.getTasks({
                    projectId,
                    ...options,
                });

                this.project.tasks = {};
                this.project.sections = {};

                sections.forEach(section => {
                    this.project.sections[section.id] = {
                        ...section,
                        tasks: {}
                    };
                })

                tasks.forEach(task => {
                    if (task.sectionId) {
                        this.project.sections[task.sectionId].tasks[task.id] = task;
                    } else {
                        this.project.tasks[task.id] = task;
                    }
                });

                // Collect garbage
                sections = null;
                tasks = null;

                window.localStorage.setItem(this.#localStorageName(projectId), JSON.stringify(this.project));

                resolve(this.project);
            } catch (error) {
                reject(error);
            }
        });
    }

    getTasks(projectId, options) {
        return new Promise(async (resolve, reject) => {
            try {
                let tasks = await this.api.getTasks({
                    ...options,
                    projectId,
                });
                
                // Clear all tasks
                Object.keys(this.project.sections).forEach(sectionId => {
                    this.project.sections[sectionId].tasks = {};
                });
                this.project.tasks = {};

                tasks.forEach(task => {
                    if (task.sectionId) {
                        this.project.sections[task.sectionId].tasks[task.id] = task;
                    } else {
                        this.project.tasks[task.id] = task;
                    }
                });

                window.localStorage.setItem(this.#localStorageName(projectId), JSON.stringify(this.project));

                resolve(this.project);
            } catch (error) {
                reject(error);
            }
        })
        
    }

    addTask(content, sectionId) {
        return new Promise(async (resolve, reject) => {
            try {
                const task = await this.api.addTask({
                    content,
                    project_id: this.project.id,
                    section_id: sectionId
                });

                resolve(task);
            } catch (error) {
                reject(error);
            }
        });
    }

    closeTask(task) {
        if (task.isCompleted) {
            return;
        }

        task.isCompleted = true;
        this.api.closeTask(task.id);

        clearTimeout(this.completeTimeout);
        this.completeTimeout = setTimeout(() => {
            window.dispatchEvent(
                new CustomEvent('sync')
            );
        }, 5000);
    }

    #localStorageName(projectId) {
        return `todoist-embed-project-${projectId}`;
    }
}