<p style="text-align: center;">
<a href="https://www.npmjs.com/package/@doist/todoist-api-typescript">
<img src="https://img.shields.io/badge/Todoist-2.1.2-E44332?logo=todoist&style=for-the-badge"/></a>
<a href="https://vue.org">
<img src="https://img.shields.io/badge/vue-3.x-4FC08D?logo=vue.js&style=for-the-badge"/></a>
<br/>
<br/>
<img src="https://i.imgur.com/lYvFo41.gif"/>
</p>

<br>

# Embeddable Todoist
You've build a beautiful and functional dashboard for your living room. But it's missing this simple yet effective thing. A synced To-do list.

<br/>

### Personal story
I've been using Todoist a long time now in my day-to-day life. It became a part of my daily routines. Since a few months I have bought my own house, and as a programmer and smart home enthusiast, a home dashboard couldn't be missing from my home.

Unfortunately, Todoist does not offer a form of embeddable project views, and a full size web page iframe embed keeps me awake at night. So I've began constructing an embeddable and simplified version of the todoist app.

<br/>

### Use
The embed comes in two forms. You can use the [online hosted version](https://decodeerheer.gitlab.io/embedded-todoist/), or you can host the single index.html file yourself. Feeling nerdy and like to make some changes? Go right ahead.

To make the tasks appear you have to supply and access key. To keep the access key private, I made use of the hashtag, which is 100% clientside. If you rather host this yourself, go right ahead and download the latest html file from the release.

When no access key was supplied, a wizard mode will help you setup the initial hash creation.

The following options are valid:

| Option | Accepted Value | Description
| ------ | -------------- | -----------
| force_dark_mode | true/false | Setting this to false will enable light mode
| project_id (required) | number | Show a project instead of a task list
| filter | string | Used to filter tasks (make sure to [encode](https://www.utilities-online.info/urlencode)!)
| input | true/false | Setting this to true will enable task input
| detail | true/false | Show due details
| locale | string | force locale (format: nl-nl) or use system/browser default
| sync_freq | number | Set update frequency time in seconds (default = 300 / 5 mins)
| no_task_text | string | Translation when no tasks available
| accent_color | hex | Color to use as accent color
| background_color | hex | Color to use as background color
| text_color | hex | Color to use as text color

When the app loads, it will first fetch all the resources it needs (projects, sections and tasks). It will construct a list the application can read. After the initial load, it will only fetch the tasks every 5 minutes. It will also sychronize when a task has been marked as completed. When a task has been marked, it will take 5 seconds before the list will be sychronized. This gives you time to check off multiple tasks without it reloading and shifting.

<br>

### Techstack
This app has been built in Vue 3 with Vite and standard CSS. Icons come in the form of SVG's, some from FontAwesome. And ofcourse the Todoist API.

<br/>

### Home Assistant
This embed has originally been made for usage in my home assistant dashboard.
Personally I use the locally hosted version. You can download the latest index.html from the releases, or build it yourself. You can upload this html file to your public directory on your machine, and start serving it from the frontend via an iframe. When using the hosted version, be aware that GitLab does not allow iframe embeds. It will work in the desktop and mobile app, just not in the browser. Download and host the index.html instead.

File Destination Example (HASSIO):<br/>
```
config/www
```

URL Example:<br/>
```
/local/todoist.html#?access_key=xxx&project_id=123
```