export default class EventBus
{
    emit(event, detail)
    {
        window.dispatchEvent(
            new CustomEvent(event, {
                detail
            })
        );
    }

    listen(event, fn)
    {
        window.addEventListener(event, fn);
    }
}